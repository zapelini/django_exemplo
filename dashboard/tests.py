from dashboard.models import Client


def test_can_be_create_client():
    Client.objects.create(name="Juca Bala", email="jucabala@enoix.com")

    clients = Client.objects.all()
    assert 1 == clients.count()
    assert 'Juca Bala' == clients.first().name
    assert 'jucabala@enoix.com' == clients.first().email
