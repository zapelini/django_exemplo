#!/bin/bash

# PROCESS_TYPE roda o worker celery
if [ "$PROCESS_TYPE" = "worker" ]; then
  celery worker -A ais_server.celery_app -n worker -l debug -Ofair
else
  python manage.py collectstatic --noinput
  gunicorn main.wsgi:application --log-level info --bind 0.0.0.0:$PORT
fi