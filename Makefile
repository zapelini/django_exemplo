.PHONY: test all
.DEFAULT_GOAL := default_target

PROJECT_NAME := django_exemplo
PYTHON_VERSION := 3.7.7
VENV_NAME := $(PROJECT_NAME)-$(PYTHON_VERSION)

help:
	@fgrep -h "##" $(MAKEFILE_LIST) | sed -e 's/\(\:.*\#\#\)/\:\ /' | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

.clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

.clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

.clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr reports/
	rm -fr .pytest_cache/
	rm -f coverage.xml

clean: .clean-build .clean-pyc .clean-test ## remove all build, test, coverage and Python artifacts

setup-dev: ## install requirements-dev.txt
	pip install -r requirements-dev.txt

setup-code-convention: ## install requirements-code-convention.txt
	pip install -r requirements-code-convention.txt

setup: ## install requirements.txt
	pip install -r requirements.txt

.create-venv: ## install python, create virtualenv and set virtualenv to current
	pyenv install -s $(PYTHON_VERSION)
	pyenv uninstall -f $(VENV_NAME)
	pyenv virtualenv $(PYTHON_VERSION) $(VENV_NAME)
	pyenv local $(VENV_NAME)

create-venv: .create-venv setup-dev ## install python, create virtualenv, set virtualenv to current and install requirements-dev.txt

## tasks
code-convention:
	flake8
	pycodestyle

test:
	pytest -v

test-cov:
	pytest -v --cov-report=term  --cov-report=html --cov=.

## database

.PHONY: db_up
db_up:
	python manage.py migrate


.PHONY: db_clean
db_clean:
	python manage.py flush


.PHONY: db_new
db_new:
	python manage.py makemigrations


.PHONY: db_version
db_version:
	python manage.py showmigrations


default_target: clean code-convention test

