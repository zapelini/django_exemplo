FROM python:3.7.7-alpine

EXPOSE 8080

WORKDIR /app

ARG arg_revision=''
ARG arg_build_date=''
ENV PORT=8080 \
    HOME=/app \
    PATH=/app/.local/bin/:$PATH \
    REVISION=$arg_revision \
    BUILD_DATE=$arg_build_date

COPY . /app

RUN apk add --no-cache bash curl && \
    apk add --no-cache --virtual .deps git make linux-headers libc-dev gcc g++ libffi-dev openssl-dev postgresql-dev && \
    make setup && \
    chmod ugo+rwx -R /app && \
    chmod ugo+x run.sh && \
    apk del .deps

CMD ./run.sh
